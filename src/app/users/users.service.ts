import { Injectable } from '@angular/core';
import {Http} from '@angular/http'; // import for http pulling data from server 
import {AngularFire} from 'angularfire2'; // import for firebase
import 'rxjs/add/operator/map';
 


@Injectable()
export class UsersService {

constructor(private _af:AngularFire, private _http:Http) { } // Declering on provate attribute af=AngularFire

usersObservable;

  addUser(user){
    this.usersObservable.push(user);
  }

  getUsers(){
    this.usersObservable = this._af.database.list('/users').map(
      gever =>{
        gever.map(
          user => {
            user.posTitles = [];
            for(var p in user.posts){
                user.posTitles.push(
                this._af.database.object('/posts/' + p)
              )
            }
          }
        );
        return gever;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
  //this.usersObservable = this._af.database.list('/users'); // if we choose object (insted of list) the syntax will be:  object('/users/1')    ('users' is the name of table in firebase)
  return this.usersObservable;
   }

   deleteUser(user){
    this.usersObservable.remove(user);
   }

   updateUser(user){
     let userKey = user.$key;
     let userData = {name:user.name, email:user.email};
     this._af.database.object('/users/'+userKey).update(userData);
   }

}
