import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
 users; 
  constructor(private _usersService: UsersService) { }

  ngOnInit() {
    this._usersService.getUsers().subscribe(usersData => {this.users = usersData; console.log(this.users)});
  }

  addUser(user){
    this._usersService.addUser(user);
  }

  deleteUser(user){
    this._usersService.deleteUser(user);
  }

  updateUser(user){
    this._usersService.updateUser(user);
  }

  goCalendar(){
    window.location.href="http://eladco.myweb.jce.ac.il/calendar/form.php";
  }

}
