import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User } from './user';    

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})

export class UserComponent implements OnInit {

user:User;
isEdit:Boolean=false;
editTextButton = "Edit";
editTextButton2 = "Delete";
@Output() userDeleteEvent = new EventEmitter<User>();
@Output() userUpdateEvent = new EventEmitter<User>();

constructor() { }

ngOnInit() {
  }

sendDelete(){
  this.userDeleteEvent.emit(this.user);
}

// sendUpdate(){
//    this.userUpdateEvent.emit(this.user);
//    this.isEdit = !this.isEdit; 
// }

toggleEdit(){
  this.isEdit = !this.isEdit;
  this.editTextButton? this.editTextButton = "Save" : this.editTextButton = "Edit";
  // this.editTextButton2? this.editTextButton2 = "Cancel" : this.editTextButton2 = "Delete";
  if(!this.isEdit){
    this.userUpdateEvent.emit(this.user);
  }
}
 
}
