import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { ProductService } from './product/product.service';
import { ProductsService } from './pruducts/products.service';

import { UserComponent } from './user/user.component';
import { FormComponent } from './form/form.component';
import { AngularFireModule } from 'angularfire2';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductComponent } from './product/product.component';
import { PruductsComponent } from './pruducts/pruducts.component';


  var config = {
    apiKey: "AIzaSyCFaa4hO2r82LLZdQPH1aNevx_y3K2q1p0",
    authDomain: "practice-84428.firebaseapp.com",
    databaseURL: "https://practice-84428.firebaseio.com",
    storageBucket: "practice-84428.appspot.com",
    messagingSenderId: "561283701502"
  };

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
 { path: 'products', component: PruductsComponent },
  { path: '', component: UsersComponent }, // default
  { path: '**', component: PageNotFoundComponent } //anything else
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    FormComponent,
    PageNotFoundComponent,
    ProductComponent,
    PruductsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,    
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(config)
  ],
  providers: [UsersService, ProductService, ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
