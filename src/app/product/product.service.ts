import { Injectable } from '@angular/core';
import {Http} from '@angular/http'; // import for http pulling data from server 
import {AngularFire} from 'angularfire2'; // import for firebase

@Injectable()
export class ProductService {
  productsObservable;

  constructor(private _af:AngularFire, private _http:Http) { }

  getProducts(){
  this.productsObservable = this._af.database.list('/product'); // if we choose object (insted of list) the syntax will be:  object('/users/1')    ('users' is the name of table in firebase)
  return this.productsObservable;
}

deleteProduct(product){
  this.productsObservable.remove(product);
}

}
