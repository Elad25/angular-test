import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { ProductService } from './product.service';
import {Product } from './product';    

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {
  product:Product;
  @Output() productDeleteEvent = new EventEmitter<Product>();

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    // this._productService.getProducts().subscribe(productsData => {this.products = productsData; console.log(this.products)});
  }
  deleteProduct(product){
   // this._productService.deleteProduct(product);
  }

  sendDelete(){
  this.productDeleteEvent.emit(this.product);
}

}
