import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-pruducts',
  templateUrl: './pruducts.component.html',
  styleUrls: ['./pruducts.component.css']
})
export class PruductsComponent implements OnInit {
products;
productsi;
  constructor(private _productsService: ProductsService) { }

  ngOnInit() {
    this._productsService.getProducts().subscribe(productsData => {this.products = productsData; console.log(this.products)});
    //this._productsService.getProducts2().subscribe(productsDatai => {this.productsi = productsDatai; console.log(this.productsi)});
  }

  deleteProduct(product){
    this._productsService.deleteProduct(product);
  }
}
