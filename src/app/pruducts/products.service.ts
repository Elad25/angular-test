import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; // import for firebase
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {
productsObservable;

  constructor(private _af:AngularFire) { }

  getProducts2(){
      this.productsObservable = this._af.database.list('/product'); // if we choose object (insted of list) the syntax will be:  object('/users/1')    ('users' is the name of table in firebase)
  return this.productsObservable;
}

deleteProduct(product){
  this.productsObservable.remove(product);
}

  getProducts(){
    this.productsObservable = this._af.database.list('/products').map(
      products =>{
        products.map(
          product => {
            product.posTitles = [];
            for(var p in product.categorys){
                product.posTitles.push(
                this._af.database.object('/categorys/' + p)
              )
            }
          }
        );
        return products;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
  return this.productsObservable;
   }

}
