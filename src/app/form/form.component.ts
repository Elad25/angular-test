import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {User} from '../user/user';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Output() userAddEvent = new EventEmitter<User>();
  user:User={name:'', email:''};

  onSubmit(form:NgForm){
    console.log(form);
    this.userAddEvent.emit(this.user);
    this.user={name:'', email:''}  
  }

  constructor() { }

  ngOnInit() {
  }

}
