import { Component } from '@angular/core';
import { AngularFire } from 'angularfire2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
    constructor(af:AngularFire) { // 'af' is like variable
    console.log(af); // we will see 'AngularFire' at the console. It's only to check if working. 
  };
}
